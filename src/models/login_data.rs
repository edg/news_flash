use crate::models::PluginID;
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum LoginData {
    Password(PasswordLogin),
    OAuth(OAuthData),
    None(PluginID),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OAuthData {
    pub id: PluginID,
    pub url: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PasswordLogin {
    pub id: PluginID,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    pub user: String,
    pub password: String,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub http_user: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub http_password: Option<String>,
}
