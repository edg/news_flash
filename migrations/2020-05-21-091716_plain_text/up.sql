ALTER TABLE articles ADD plain_text TEXT default null;

UPDATE articles SET plain_text = summary;

DROP TABLE fts_table;

CREATE VIRTUAL TABLE IF NOT EXISTS fts_table USING fts4 (content='articles', article_id, plain_text, title, author);

INSERT INTO fts_table(fts_table) VALUES('rebuild');